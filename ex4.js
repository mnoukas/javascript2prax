(function () {
    'use strict';

    var ul = document.getElementById('list');
    var button = document.getElementById('button1');
    var input = document.getElementById('input1');

    var counter = 1;
    var items = [new Item(counter++, 'item 1'), new Item(counter++, 'item 2')];

    render();

    function render() {
        while (ul.hasChildNodes()){
            ul.removeChild(ul.lastChild);
        }
        for (var i = 0; i < items.length; i++){
            addLi(items[i]);
        }
    }

    button.onclick = function () {
       addItem();
       var code = input.value;
    };

    function deleteItemz(id){
        items = items.filter(function (each){
            return each.id !== id;

        });
        render();
    }

    /*
    function deleteItem(id){
        for (var i = 0; i < items.length; i++){
            if (items[i].id === id){
                delete items[i];
            }
        }
    }
    */

    function addItem(){
        items.push(new Item(counter++, input.value));
        render();
    }

    function Item(id, text) {
        this.id = id;
        this.text = text;
    }

    function addLi(item) {
        var li = document.createElement('li');
        li.innerText = item.text;
        ul.appendChild(li);

        var b = document.createElement('button');
        b.innerText = 'X';
        b.onclick = function () {
            deleteItemz(item.id)
        };
        li.append(b);
    }
})();
